Release Notes
===================

SIP Server 1.0.0 (2020-04-02)
---------------------------------

* 🐛 Fix init_domain script
* 📝 Rewrite README
* 🐳 Fix typo in docker-compose.yml
* 📄 Convert LICENSE to Markdown
* 📝 Add AUTHORS and CODEOWNERS
* 🙈 Update .gitignore

SIP Server 0.0.1 (2020-04-02)
---------------------------------

* 🎉 Initial state of the forked repo
